/**
 * Voici mon test sur l'algo de tarif d'assurance fait en cours
 * Celui ci permet de calculer le tarif attribué à un conducteur en fonction de plusieures conditions
 */

//Formulaire

const form = document.querySelector("form");

form.addEventListener('submit', (e) => {
    e.preventDefault()

    let personne = {
        name: e.target.nom.value,
        age: e.target.age.value,
        permis: e.target.permis.value,
        accidents: e.target.accident.value,
        fidélitée: e.target.fidélitée.value
    }

    let result = getTarif(personne)

    var resultat = document.getElementById("resultat")
    resultat.innerHTML = result
})








/**
 * Ensuite, je fais mes fonctions qui s'occuperont de faire tous les tests
 */

function getAge(personne) {
    if (personne.age > 25) {
        console.log(personne.name + " a plus de 25ans");
        return true
    } else {
        console.log(personne.name + " a moins de 25ans");
        return false
    }
}


function getPermis(personne) {
    if (personne.permis > 2) {
        console.log(personne.name + " a le permis depuis plus de 2 ans");
        return true
    } else {
        console.log(personne.name + " a le permis depuis moins de 2 ans");
        return false
    }
}

function getAccidents1(personne) {
    if (personne.accidents < 1) {
        console.log(personne.name + " n'a jamais u d'accidents");
        return true
    } else {
        console.log(personne.name + " a u plus d'un accident");
        getAccidents2(personne)
        return false
    }
}

function getAccidents2(personne) {
    if (personne.accidents < 2) {
        console.log(personne.name + " n'a u qu'un seul accident");
        return true
    } else {
        console.log(personne.name + " a u plus de deux accidents");
        getAccidents3(personne)
        return false
    }
}

function getAccidents3(personne) {
    if (personne.accidents < 3) {
        console.log(personne.name + " n'a u que deux accidents");
        return true
    } else {
        console.log(personne.name + " a u plus de trois accidents");
        return false
    }
}


function getFidélitée(personne) {
    if (personne.fidélitée > 1) {
        console.log(personne.name + " est fidéle");
        return true
    } else {
        console.log(personne.name + " est un nouveau client");
        return false
    }
}


/**
 * Et bien sur une groooooosse fonction d'enchainement de if et d'appel sur  les fonctions au dessus pour calculer tout ça et rendre le résultat dans la console
 * @param {*} personne représente une personne on peut modifier ses attributs tout en haut du fichier
 */


function getTarif(personne) {
    if (getAge(personne) == true) {
        if (getPermis(personne) == true) {
            if (getAccidents1(personne) == true) {
                if (getFidélitée(personne) == true) {
                    return personne.name + " aura le tarif A"
                } else {
                    return personne.name + " aura le tarif B";
                }
            } else {
                if (getAccidents2(personne) == true) {
                    if (getFidélitée(personne) == true) {
                        return personne.name + " aura le tarif B";
                    } else {
                        return personne.name + " aura le tarif C";
                    }
                } else {
                    if (getAccidents3(personne) == true) {
                        if (getFidélitée(personne) == true) {
                            return personne.name + " aura le tarif C";
                        } else {
                            return personne.name + " aura le tarif D";
                        }
                    } else {
                        if (getFidélitée(personne) == true) {
                            return personne.name + " aura le tarif D";
                        } else {
                            return personne.name + " est refusé";
                        }
                    }
                }
            }
        } else {
            if (getAccidents1(personne) == true) {
                if (getFidélitée(personne) == true) {
                    return personne.name + " aura le tarif B";
                } else {
                    return personne.name + " aura le tarif C";
                }
            } else {
                if (getAccidents2(personne) == true) {
                    if (getFidélitée(personne) == true) {
                        return personne.name + " aura le tarif C";
                    } else {
                        return personne.name + " aura le tarif D";
                    }
                } else {
                    if (getFidélitée(personne) == true) {
                        return personne.name + " aura le tarif D";
                    } else {
                        return personne.name + " est refusé";
                    }
                }
            }
        }
    } else {
        if (getPermis(personne) == true) {
            if (getAccidents1(personne) == true) {
                if (getFidélitée(personne) == true) {
                    return personne.name + " aura le tarif B";
                } else {
                    return personne.name + " aura le tarif C";
                }
            } else {
                if (getAccidents2(personne) == true) {
                    if (getFidélitée(personne) == true) {
                        return personne.name + " aura le tarif C";
                    } else {
                        return personne.name + " aura le tarif D";
                    }
                } else {
                    if (getFidélitée(personne) == true) {
                        return personne.name + " aura le tarif D";
                    } else {
                        return personne.name + " est refusé";
                    }
                }
            }
        } else {
            if (getAccidents1(personne) == true) {
                if (getFidélitée(personne) == true) {
                    return personne.name + " aura le tarif C";
                } else {
                    return personne.name + " aura le tarif D";
                }
            } else {
                return personne.name + " est refusé";
            }
        }
    }
}



